As a user, 
I want to see current application version on a home page of the server
Version should be configurable and taken from (some variable in code/application.properties/some file in the same folder as application jar)
If the version could not be found, I want to see some fallback value on the page, and a message describing the problem below it