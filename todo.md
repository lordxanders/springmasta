What we want to create during the course of this tutorial project:
1) Working spring-based web-server, serving requests on different REST controllers
2) Model of entities in some domain (banking, some game, HR, etc.)
3) CRUD for your model
4) Showing model to the user via UI, letting him interact with it

Current tasks:
1) Launch the project, check default spring output, no errors expected
2) Find where the server is deployed after launch (check spring logs + application.properties file), try to access it, check what files and packages are already present in the project
3) And now you should draw the whole owl: implement STORY 1 (described in a separate file), do it in a separate branch (name it 'feature/show_version', e.g), commit it and make a pull-request to the master branch
4) Google and try basic tutorials about @Entity and CrudRepository in Spring
5) Implement STORY 2, in a new separate branch.


STAGE II*:
1) Thinking through the EAV model (metamodel) - what it is, why we may want to use it, pros/cons
2) Designing model to support it (tables, attributes)
3) Implementing it as some MutableDataObject
4) UI for it (TableView, CtrlView)