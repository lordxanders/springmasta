As an owner of some betting company, 
I want to track data about all of our users.
It should contain their unique id and email.
I also want to see the number of such users on our homepage.
But since we are still a small startup and don't have many of them, I want to have a button that will create and store a new user.
To make it less suspicious for our competitors, I want generated users to have human-like emails,
that should be randomly made up from predefined lists of words and domains


//Tips & Assumptions
Predefined lists of words - like it's done in URL 'https://www.twitch.tv/pokimane/clip/ObeseImpartialPigNotATK'
And should look like a legit email.
Don't need to check if such email already exists in our DB, uniqueness of ID would suffice
