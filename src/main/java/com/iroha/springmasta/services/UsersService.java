package com.iroha.springmasta.services;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import com.iroha.springmasta.repositories.UsersRepository;
import com.iroha.springmasta.users.User;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;

@Service
public class UsersService {

  //Fake first names for id generation
  static final String[] FIRST_NAME = new String[]{
    "ivan",
    "dmitrii",
    "oleksandr",
    "artyom",
    "petr",
    "anton",
    "viktor",
    "igor"
  };

  //Fake last names for id generation
  static final String[] LAST_NAME = new String[]{
    "ivanov",
    "petrov",
    "aleksandrov",
    "antonov",
    "petrov",
    "egorov",
    "viktorov",
    "sidorov"
  };

  //Real email services for email generation
  static final String[] EMAIL_DOMAINS = new String[]{
    "@gmail.com",
    "@ya.ru",
    "@rambler.ru",
    "@mail.ru"
  };

  @Autowired
  private UsersRepository usersRepository;

  public void addUser(User user){
    usersRepository.save(user);
  }

  public List<User> getAllUsers(){
    List<User> users = new ArrayList<>();
    for(User u : usersRepository.findAll()){
        users.add(u);
    }
    return users;
  }

  public long getNumOfUsers(){
    return usersRepository.count();
  }

  public String generateEmail(){
    Random rand = new Random();
    String email = FIRST_NAME[rand.nextInt(FIRST_NAME.length)] + LAST_NAME[rand.nextInt(LAST_NAME.length)] + EMAIL_DOMAINS[rand.nextInt(EMAIL_DOMAINS.length)];
    return email;
  }
}
