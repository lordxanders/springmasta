package com.iroha.springmasta.repositories;

import org.springframework.data.repository.CrudRepository;

import com.iroha.springmasta.users.User;

public interface UsersRepository extends CrudRepository<User, Long>{

}
