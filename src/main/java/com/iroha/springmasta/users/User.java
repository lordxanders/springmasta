package com.iroha.springmasta.users;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Entity
public class User {

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private long id;
  private String email;

  protected User() {

  }

  public User(String email) {
    this.email = email;
  }

  public long getId(){
    return id;
  }

  public String getEmail(){
    return email;
  }

  public void setEmail(String email){
    this.email = email;
  }
}
