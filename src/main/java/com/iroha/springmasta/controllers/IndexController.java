package com.iroha.springmasta.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.io.BufferedReader;
import java.io.FileReader;

@RestController
public class IndexController {

    private static final String FILE = "target/version.txt";

    @GetMapping("/")
    public ModelAndView home() {
      return new ModelAndView("home.html");
    }

    @GetMapping("/version")
    public String getVersion() {
      try(BufferedReader reader = new BufferedReader(new FileReader(FILE))) {
          return reader.readLine();
      } catch(Exception e){
          return e.getMessage();
      }
    }
}
