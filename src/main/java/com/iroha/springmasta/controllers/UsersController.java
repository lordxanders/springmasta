package com.iroha.springmasta.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.ModelAndView;

import com.iroha.springmasta.users.User;
import com.iroha.springmasta.services.UsersService;

import java.util.List;

@RestController
public class UsersController {

  @Autowired
  private UsersService usersService;

  //Shows all users that are present in a database
  @GetMapping("/users")
  public List<User> getAllUsers() {
    return usersService.getAllUsers();
  }

  //Processes GET-request (see masta.js) for a number of users in a database
  @GetMapping("/numofusers")
  public long getNumOfUsers(){
    return usersService.getNumOfUsers();
  }

  @GetMapping("/button")
  public ModelAndView showForm(){
    return new ModelAndView("button.html");
  }

  //Generates and saves user in a database after button press
  @PostMapping("/button")
  public void submitForm(){
    String email = usersService.generateEmail();
    usersService.addUser(new User(email));
  }
}
